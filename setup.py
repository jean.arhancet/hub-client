import setuptools

setuptools.setup(
    name="hubclient",  # Replace with your own username
    version="0.0.1",
    author="Jean Arhancetebehere",
    author_email="jean.arhancetebehere@veolia.com",
    description="Hub Python client",
    packages=setuptools.find_packages(),
    install_requires=[
        "google-auth==1.24.0",
        "pydantic==1.7.3",
        "email-validator==1.1.2",
        "pytest-mock==3.3.1"
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
