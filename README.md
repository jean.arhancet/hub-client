# Hub Python client

This is a simple hub python client

## Installation

```text
 pip install hubclient --extra-index-url https://__token__:<personal_gitlab_token>@gitlab.com/api/v4/projects/23208108/packages/pypi/simple
```

## Features

- Retrieve GCP environnement
- Retrieve Users, Communities, Applications

## Authentication

This lib generates a JWT token with `id_token` of google.oauth2 :

```text
1. If the application is running in Compute Engine, App Engine or Cloud Run,
   then the ID token are obtained from the metadata server.
2. If the environment variable ``GOOGLE_APPLICATION_CREDENTIALS`` is set
   to the path of a valid service account JSON file, then ID token is
   acquired using this service account credentials.
3. If metadata server doesn't exist and no valid service account credentials
   are found, :class:`~google.auth.exceptions.DefaultCredentialsError` will
   be raised.
```

## A Simple Example

```py
from hub.client import HubClient
from hub.types import UserParams

client = HubClient()

user_params = UserParams(app_key='<APP_KEY>', community_key='<COMMUNITY_KEY>')

for user in client.get_users(user_params):
    print(user)
```
