import logging
from typing import List

import requests

from hub.utils import get_google_cloud_env, IapAuth
from hub.types import UserParams, UserPage, User, EntityParams, Entity, EntityPage, Community, \
    CommunityParams, CommunityPage, ApplicationParams


class HubClient:

    def __init__(self, service_hostname: str = None, client_id: str = None,
                 session: requests.Session = None):
        if not service_hostname and not client_id:
            client_id, service_hostname = get_google_cloud_env()

        if not session:
            session = requests.Session()

        self.session = session
        self.service_hostname = service_hostname
        self.client_id = client_id

    def get_users(self, user_params: UserParams) -> List[User]:
        try:
            users: List[User] = list()
            while True:
                url = 'https://{}/api/rest/v1/users'.format(self.service_hostname)
                page = self.session.get(url, params=user_params.dict(),
                                        auth=IapAuth(self.client_id))
                page.raise_for_status()
                user_page = UserPage(**page.json())
                users.extend(user_page.users)
                if user_page.next_page_token is None:
                    break
                user_params.page_token = user_page.next_page_token
            return users
        except Exception as err:
            logging.error('Failed to retrieve users : ' + str(err))
            return list()

    def get_entities(self, entity_params: EntityParams) -> List[Entity]:
        try:
            entities: List[Entity] = list()
            while True:
                url = 'https://{}/api/rest/v1/orgs'.format(self.service_hostname)
                page = self.session.get(url, params=entity_params.dict(),
                                        auth=IapAuth(self.client_id))
                page.raise_for_status()
                entity_page = EntityPage(**page.json())
                entities.extend(entity_page.entities)
                if entity_page.next_page_token is None:
                    break
                entity_params.page_token = entity_page.next_page_token
            return entities
        except Exception as err:
            logging.error('Failed to retrieve entities : ' + str(err))
            return list()

    def get_communities(self, community_params: CommunityParams) -> List[Community]:
        try:
            url = 'https://{}/api/rest/v1/communities'.format(self.service_hostname)
            communities = self.session.get(url, params=community_params.dict(),
                                           auth=IapAuth(self.client_id))
            return CommunityPage(**communities.json()).communities
        except Exception as err:
            logging.error('Failed to retrieve communities : ' + str(err))
            return list()

    def get_applications(self, application_params: ApplicationParams) -> List[Community]:
        try:
            url = 'https://{}/api/rest/v1/application/{}/communities'.format(self.service_hostname,
                                                                             application_params.app_key)
            communities = self.session.get(url, auth=IapAuth(self.client_id))
            return CommunityPage(**communities.json()).communities
        except Exception as err:
            logging.error('Failed to retrieve applications : ' + str(err))
            return list()
