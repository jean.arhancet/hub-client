import os
from enum import Enum
from typing import Tuple

from google.auth.transport.requests import Request
from google.oauth2 import id_token
from requests.auth import AuthBase


class Env(Enum):
    DEV = 'dev'
    UAT = 'uat'
    PROD = 'prod'


iap_config_by_env = {
    Env.DEV: {
        'client_id': '1073417348762-rjgaufti9ebuelt9t0uffc3ctdt83ao9.apps.googleusercontent.com',
        'hostname': 'gbl-im-ve-waternamicshub-test.appspot.com'
    }, Env.UAT: {
        'client_id': '1073417348762-rjgaufti9ebuelt9t0uffc3ctdt83ao9.apps.googleusercontent.com',
        'hostname': 'gbl-im-ve-waternamicshub-test.appspot.com'
    }, Env.PROD: {
        'client_id': '730143494590-c4okjhcksj6loj91kckmgqomtogcs21e.apps.googleusercontent.com',
        'hostname': 'gbl-im-ve-waternamicshub.appspot.com'
    }
}


def get_jwt_token(client_id) -> str:
    return id_token.fetch_id_token(Request(), client_id)


def get_google_cloud_env() -> Tuple[str, str]:
    """
    APPLICATION_ID is retrieved in Google Cloud infra
    This function retrieve client id and hostname for Hub with check if the current project is DEV,UAT OR PROD
    with naming Veolia convention (<project>-dev, <project>-uat and <project>)
    GCP ENV VARIABLE :
    APPLICATION_ID => APP ENGINE, COMPUTE
    GCP_PROJECT => CLOUD FUNCTIONS
    @rtype: object
    """
    project_id = os.getenv('APPLICATION_ID')
    if not project_id:
        project_id = os.getenv('GCP_PROJECT', Env.DEV)
    matching = [env for env in Env if env.value in project_id]
    if not matching:
        matching = [Env.PROD]
    iap_config = iap_config_by_env[matching[0]]
    return iap_config['client_id'], iap_config['hostname']


class IapAuth(AuthBase):
    """IAP Authentication."""

    def __init__(self, client_id):
        # setup any auth-related data here
        self.client_id = client_id

    def __call__(self, r):
        # modify and return the request
        r.headers['Authorization'] = "Bearer {}".format(get_jwt_token(self.client_id))
        return r
