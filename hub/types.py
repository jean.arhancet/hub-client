from datetime import datetime, date
from typing import Optional, List

from pydantic import BaseModel, EmailStr, validator


class Entity(BaseModel):
    external_id: str
    external_type: Optional[str]
    key: str
    label: Optional[str]
    level_number: Optional[int]
    name: str
    parent_key: Optional[str]


class User(BaseModel):
    id: str
    email: EmailStr
    create_date: Optional[datetime]
    created_by: str
    last_modification_date: Optional[datetime]
    modified_by: Optional[str]
    entities: List[Entity]
    role: Optional[str]
    status: str


class UserPage(BaseModel):
    next_page_token: Optional[str]
    users: List[User]


class UserParams(BaseModel):
    app_key: str
    community_key: str
    updated_after: Optional[str]
    updated_before: Optional[str]
    page_token: Optional[int]

    @validator('updated_after', 'updated_before', pre=True)
    def parse_date_to_str(cls, v):
        if isinstance(v, date):
            return v.strftime("%Y-%m-%dT%H:%M:%S")
        return v


class EntityPage(BaseModel):
    next_page_token: Optional[str]
    entities: List[Entity]


class EntityParams(BaseModel):
    level_number: int
    community_key: str
    search: Optional[str]
    parent_key: Optional[str]
    items_per_page: Optional[int]
    page_token: Optional[str]


class Community(BaseModel):
    external_id: Optional[str]
    key: str
    name: str
    veolia_org: Optional[str]
    number_entities_level: Optional[int]


class CommunityPage(BaseModel):
    communities: List[Community]


class CommunityParams(BaseModel):
    external_id: Optional[str]


class ApplicationParams(BaseModel):
    app_key: str
