from datetime import datetime

from pytest_mock import MockerFixture
from requests import Session, Response

from hub.client import HubClient
from hub.types import UserParams, UserPage, User, Entity, EntityPage, EntityParams, CommunityParams, \
    Community, CommunityPage, ApplicationParams


def side_effect_users(*args, **kwargs) -> Response:
    entities = [Entity(external_id='1', external_type='type', key='key', label='label', level_number=1, name='Entity',
                       parent_key='parent')]
    response = Response()
    response.status_code = 200
    if kwargs['params']['page_token'] is None:
        response._content = bytes(UserPage(next_page_token='2', users=[
            User(id='1', email='alain.dupond@veolia.com', create_date=datetime.now(), created_by='Alain Dupont',
                 entities=entities,
                 role='User', status='Active')]).json(), 'utf-8')

    if kwargs['params']['page_token'] == '2':
        response._content = bytes(UserPage(next_page_token=None, users=[
            User(id='1', email='marc.dupont@veolia.com', create_date=datetime.now(), created_by='Alain Dupond',
                 entities=entities,
                 role='User', status='Active')]).json(), 'utf-8')
    return response


def side_effect_entities(*args, **kwargs) -> Response:
    entities = [Entity(external_id='1', external_type='type', key='key', label='label', level_number=1, name='Entity',
                       parent_key='parent')]
    response = Response()
    response.status_code = 200
    if kwargs['params']['page_token'] is None:
        response._content = bytes(EntityPage(next_page_token='2', entities=entities).json(), 'utf-8')
    if kwargs['params']['page_token'] == '2':
        entities[0].external_id = '2'
        response._content = bytes(EntityPage(next_page_token=None, entities=entities).json(), 'utf-8')
    return response


def test_get_users_if_hub_return(mocker: MockerFixture) -> None:
    mock_get = mocker.patch.object(Session, 'get')
    mock_get.side_effect = side_effect_users

    client = HubClient(service_hostname='gcp@appspot.com', client_id='gcp_client_id', session=Session())
    users = client.get_users(UserParams(app_key='module', community_key='community'))
    assert len(users) == 2


def test_get_users_if_hub_return_error(mocker: MockerFixture) -> None:
    mock_get = mocker.patch.object(Session, 'get')
    mock_get.return_value = mocker.PropertyMock(status_code=500)

    client = HubClient(service_hostname='gcp@appspot.com', client_id='gcp_client_id', session=Session())
    users = client.get_users(UserParams(app_key='module', community_key='community'))
    assert len(users) == 0


def test_get_entities_if_hub_return(mocker: MockerFixture) -> None:
    mock_get = mocker.patch.object(Session, 'get')
    mock_get.side_effect = side_effect_entities
    client = HubClient(service_hostname='gcp@appspot.com', client_id='gcp_client_id', session=Session())
    entities = client.get_entities(EntityParams(level_number=1, community_key='community'))
    assert len(entities) == 2


def test_get_entities_if_hub_return_error(mocker: MockerFixture) -> None:
    mock_get = mocker.patch.object(Session, 'get')
    mock_get.return_value = mocker.PropertyMock(status_code=500)

    client = HubClient(service_hostname='gcp@appspot.com', client_id='gcp_client_id', session=Session())
    entities = client.get_entities(EntityParams(level_number=1, community_key='community'))
    assert len(entities) == 0


def test_get_communities_if_hub_return(mocker: MockerFixture) -> None:
    community = Community(external_id='1', key='key', name='name', veolia_org='org', number_entities_level=1)
    mock_get = mocker.patch.object(Session, 'get')
    response = Response()
    response.status_code = 200
    response._content = bytes(CommunityPage(communities=[community]).json(), 'utf-8')
    mock_get.return_value = response
    client = HubClient(service_hostname='gcp@appspot.com', client_id='gcp_client_id', session=Session())
    communities = client.get_communities(CommunityParams())
    assert len(communities) == 1


def test_get_communities_if_hub_return_error(mocker: MockerFixture) -> None:
    mock_get = mocker.patch.object(Session, 'get')
    mock_get.return_value = mocker.PropertyMock(status_code=500)

    client = HubClient(service_hostname='gcp@appspot.com', client_id='gcp_client_id', session=Session())
    communities = client.get_communities(CommunityParams())
    assert communities == list()


def test_get_application_if_hub_return(mocker: MockerFixture) -> None:
    community = Community(external_id='1', key='key', name='name', veolia_org='org', number_entities_level=1)
    mock_get = mocker.patch.object(Session, 'get')
    response = Response()
    response.status_code = 200

    response._content = bytes(CommunityPage(communities=[community]).json(), 'utf-8')
    mock_get.return_value = response
    client = HubClient(service_hostname='gcp@appspot.com', client_id='gcp_client_id', session=Session())
    applications = client.get_communities(ApplicationParams(app_key='app_key'))
    assert len(applications) == 1


def test_get_application_if_hub_return_error(mocker: MockerFixture) -> None:
    mock_get = mocker.patch.object(Session, 'get')
    mock_get.return_value = mocker.PropertyMock(status_code=500)

    client = HubClient(service_hostname='gcp@appspot.com', client_id='gcp_client_id', session=Session())
    applications = client.get_communities(ApplicationParams(app_key='app_key'))
    assert applications == list()
