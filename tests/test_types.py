from datetime import datetime

from hub.types import UserParams


def test_user_param_with_date() -> None:
    user_params = UserParams(app_key='1', community_key='1',
                             updated_after=datetime(year=1995, month=2, day=14, hour=13, minute=00))
    user_params_dict = user_params.dict()
    assert user_params_dict == {'app_key': '1', 'community_key': '1', 'page_token': None,
                                'updated_after': '1995-02-14T13:00:00', 'updated_before': None}


def test_user_param_with_string() -> None:
    user_params = UserParams(app_key='1', community_key='1',
                             updated_after='1995-02-14T13:00:00')
    user_params_dict = user_params.dict()
    assert user_params_dict == {'app_key': '1', 'community_key': '1', 'page_token': None,
                                'updated_after': '1995-02-14T13:00:00', 'updated_before': None}
