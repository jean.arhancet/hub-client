import os

from hub.utils import get_google_cloud_env


def test_retrieve_dev_env_if_gcp_infra_is_dev() -> None:
    os.environ['APPLICATION_ID'] = 'hub-client-dev'
    _, hostname = get_google_cloud_env()
    assert 'gbl-im-ve-waternamicshub-test.appspot.com' == hostname


def test_retrieve_dev_env_if_cloud_function_is_in_gcp_infra_dev() -> None:
    os.environ['GCP_PROJECT'] = 'hub-client-dev'
    _, hostname = get_google_cloud_env()
    assert 'gbl-im-ve-waternamicshub-test.appspot.com' == hostname


def test_retrieve_uat_env_if_gcp_infra_is_uat() -> None:
    os.environ['APPLICATION_ID'] = 'hub-client-uat'
    _, hostname = get_google_cloud_env()
    assert 'gbl-im-ve-waternamicshub-test.appspot.com' == hostname


def test_retrieve_env_if_not_gcp_infra() -> None:
    _, hostname = get_google_cloud_env()
    assert 'gbl-im-ve-waternamicshub-test.appspot.com' == hostname


def test_retrieve_prod_env_if_gcp_infra_is_prod() -> None:
    os.environ['APPLICATION_ID'] = 'hub-client'
    _, hostname = get_google_cloud_env()
    assert 'gbl-im-ve-waternamicshub.appspot.com' == hostname
